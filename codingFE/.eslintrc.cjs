module.exports = {
  env: { browser: true, es2020: true },
  extends: [
    "eslint:recommended",
    "xo",
    "plugin:react/recommended",
    "plugin:react-hooks/recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:prettier/recommended",
  ],
  overrides: [
    {
      extends: ["xo-typescript"],
      files: ["*.ts", "*.tsx"],
    },
  ],
  parser: "@typescript-eslint/parser",
  parserOptions: { ecmaVersion: "latest", sourceType: "module" },
  plugins: ["react", "react-refresh"],
  rules: {
    "react-refresh/only-export-components": "warn",
    "prettier/prettier": [
      "error",
      {
        printWidth: 80,
        endOfLine: "lf",
        singleQuote: true,
        tabWidth: 2,
        indentStyle: "space",
        useTabs: true,
        trailingComma: "es5",
      },
    ],
    // Disallow the `any` type.
    "@typescript-eslint/no-explicit-any": "warn",
    "no-console": "warn",
  },
};
